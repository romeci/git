# Teste WebProdutora

O projeto consiste em reproduzir a [imagem](https://bitbucket.org/romeci/git/raw/d0f0247b7d106f1fc1b39325e17de4cdc9d6bc0f/desafios/webprodutora/layout.png) anexada utilizando html e css.

##### Recursos

  - [Bootstrap 4.5](https://getbootstrap.com/docs/4.5)
  - HTML
  - CSS 3


* Link do Projeto [https://git.romeci.com.br/desafios/webprodutora/](https://git.romeci.com.br/desafios/webprodutora/)